from pyspark.sql import SparkSession, functions as f

#=============================================QUERY12===========================================================================

def executeQ12RDD():
    sc = SparkSession.builder.appName("Python Spark SQL basic example").config("spark.some.config.option", "some-value").getOrCreate()

    authpapers_df=sc.read.format("csv").option("header", "true").load("AuthPapers.csv")
    authpapers_df1=sc.read.format("csv").option("header", "true").load("AuthPapers.csv")
    persons_df=sc.read.format("csv").option("header", "true").load("Persons.csv")

    pairs= authpapers_df.join(authpapers_df1, (authpapers_df.pkey == authpapers_df1.pkey) &
         (authpapers_df.akey!=authpapers_df1.akey) & (authpapers_df.akey>authpapers_df1.akey))\
        .select(authpapers_df["akey"],authpapers_df1["akey"],authpapers_df["pkey"])\
        .groupby(authpapers_df["akey"].alias('a1'),authpapers_df1["akey"].alias('a2'))\
        .agg(f.count(authpapers_df["pkey"]).alias('ct')) \
        .orderBy('ct', ascending=False)

    tot1=pairs.join(persons_df,(pairs.a1 == persons_df.akey)).select(persons_df["name"].alias("a1"),pairs["a2"],pairs["ct"])

    tot2_df=tot1.join(persons_df,(tot1.a2 == persons_df.akey)).select(tot1["a1"],persons_df["name"].alias("a2"),tot1["ct"]).limit(5)

    tot2_df.write.format('parquet').save('out12.parquet')
    sc.stop()

#=============================================QUERY13===========================================================================

def executeQ13RDD():
    sc = SparkSession.builder.appName("Python Spark SQL basic example").config("spark.some.config.option", "some-value").getOrCreate()
    papers_df=sc.read.format("csv").option("header", "true").load("Papers.csv")
    conf_df=sc.read.format("csv").option("header", "true").load("Conferences.csv")
    journ_df=sc.read.format("csv").option("header", "true").load("Journals.csv")
    authpapers_df=sc.read.format("csv").option("header", "true").load("AuthPapers.csv")
    persons_df=sc.read.format("csv").option("header", "true").load("Persons.csv")

    conf_pkey_df=papers_df.join(conf_df,(papers_df.ckey == conf_df.ckey) &
                                (conf_df.sname=='SIGMOD') & (conf_df.year>=2014) & (conf_df.year<=2020)).select(papers_df.pkey)

    journ_pkey_df=papers_df.join(journ_df,(papers_df.jkey == journ_df.jkey) &
                                (journ_df.sname=='PVLDB') & (journ_df.year>=2014) & (journ_df.year<=2020)).select(papers_df.pkey)

    all_papers_df=conf_pkey_df.union(journ_pkey_df)

    auths_df=authpapers_df.join(all_papers_df,authpapers_df.pkey==all_papers_df.pkey).select(authpapers_df.akey,all_papers_df.pkey)\
        .groupby(authpapers_df.akey).agg(f.count(all_papers_df.pkey).alias('ct'))

    fin_df=auths_df.join(persons_df,(auths_df.akey == persons_df.akey)).select(persons_df["name"],auths_df["ct"].alias("count"))

    final_df=fin_df.filter(auths_df.ct>20).orderBy(auths_df.ct, ascending=False)
    final_df.write.format('parquet').save('out13.parquet')
    sc.stop()

if __name__ == "__main__":
    executeQ12RDD()
    executeQ13RDD()
