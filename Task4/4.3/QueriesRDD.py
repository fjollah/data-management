from pyspark import SparkContext, SparkConf


#==============================================Custom Functions=====================================================
def compare(tup):
    if(tup[1][0]!=tup[1][1]):
        return tup

def sort(x):
    ls=list(x)
    ls.sort()
    return tuple(ls)

def replace(id,per_dict):
    for k,v in per_dict.items():
     if(k==id):
         id=v
    return id

def checkNull(tup):
    if(tup[3]!=''):
        return tup

def checkNull1(tup):
    if(tup[4]!=''):
        return tup

def rm_t(x):
    if(x[1]=='SIGMOD' and 2014 <= int(x[2]) <= 2020):
        return x

def rm_t1(x):
    if(x[1]=='PVLDB' and 2014 <= int(x[2]) <= 2020):
        return x

#===================================================QUERY12=========================================================

def executeQ12RDD():

    sc = SparkContext("local", "first")
    persons = sc.textFile("Persons.csv").filter(lambda x:x!="akey,name,website,ikey")\
        .map(lambda line: line.split(",")) \
        .map(lambda line: (line[0],line[1]))

    authpapers=sc.textFile("AuthPapers.csv") \
        .map(lambda line: line.split(",")) \
        .map(lambda line: (line[0],line[1])) \

    authpapers1=sc.textFile("AuthPapers.csv") \
        .map(lambda line: line.split(",")) \
        .map(lambda line: (line[0],line[1])) \

    per_dict =persons.collectAsMap()

    res_join = authpapers.join(authpapers1).filter(lambda x:compare(x))\
        .map(lambda x:(sort(x[1]),x[0])).groupByKey().map(lambda x:(x[0],len(set(x[1])))).sortBy(lambda x:-x[1])\

    fin_res=res_join.map(lambda x:(replace(x[0][0],per_dict),replace(x[0][1],per_dict),x[1])).take(5)
    print(*fin_res,sep="\n")
    sc.stop()

#===================================================QUERY13=========================================================

def executeQ13RDD():

    sc = SparkContext("local", "second")
    papers_ckey = sc.textFile("Papers.csv").filter(lambda x:x!="pkey,title,pages,ckey,jkey")\
        .map(lambda line: line.split(",")).filter(lambda x:checkNull(x)).map(lambda x:(int(x[3]),int(x[0])))

    papers_jkey = sc.textFile("Papers.csv").filter(lambda x:x!="pkey,title,pages,ckey,jkey") \
    .map(lambda line: line.split(",")).filter(lambda x:checkNull1(x)).map(lambda x:(int(x[4]),int(x[0])))

    confs=sc.textFile("Conferences.csv").filter(lambda x:x!="ckey,sname,title,city,cokey,year,isbn")\
        .map(lambda line: line.split(",")).map(lambda x:(int(x[0]),x[1],x[5])).filter(lambda x:rm_t(x))

    journ=sc.textFile("Journals.csv").filter(lambda x:x!="jkey,sname,title,volume,issue,year")\
        .map(lambda line: line.split(",")).map(lambda x:(int(x[0]),x[1],x[5])).filter(lambda x:rm_t1(x))

    authpapers=sc.textFile("AuthPapers.csv").filter(lambda x:x!="pkey,akey,rank") \
        .map(lambda line: line.split(",")) .map(lambda line: (int(line[0]),int(line[1])))

    persons = sc.textFile("Persons.csv").filter(lambda x:x!="akey,name,website,ikey")\
        .map(lambda line: line.split(",")).map(lambda line: (int(line[0]),line[1]))

    con_pkey= confs.join(papers_ckey).filter(lambda x:set(x)).map(lambda x:(x[0],x[1][0],x[1][1]))
    jour_pkey= journ.join(papers_jkey).filter(lambda x:set(x)).map(lambda x:(x[0],x[1][0],x[1][1]))

    tot_pkey=con_pkey.union(jour_pkey).map(lambda x:(x[2],x[1]))

    auth_pkey=authpapers.join(tot_pkey).map(lambda x:(x[1][0],x[0])).groupByKey()\
        .map(lambda x:(x[0],set(x[1]))).filter(lambda x: len(x[1])>20).map(lambda x:(x[0],len(x[1])))\

    final_res=persons.join(auth_pkey).map(lambda x:(x[1][0],x[1][1])).sortBy(lambda x:-x[1]).collect()
    print(*final_res,sep="\n")
    sc.stop()
#============================================MAIN==========================================================

if __name__ == "__main__":
    executeQ12RDD()
    executeQ13RDD()