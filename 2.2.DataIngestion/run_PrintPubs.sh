#/bin/sh
#installs psycopg2 library, in case it is not installed
pip install psycopg2-binary

# E.g bash run_PrintPubs.sh "Denis Helic" localhost 5432 db11831695 postgres 123
#NOTE: There are extra arguments in case the author name has more words
python PrintPubs.py $1 $2 $3 $4 $5 $6 $7 $8 $9 ${10} ${11} ${12} ${13} ${14}


