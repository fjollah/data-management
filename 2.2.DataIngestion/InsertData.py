#! /usr/bin/env python3

import sys, csv, psycopg2

db_host = sys.argv[-5]
db_port = sys.argv[-4]
db_name = sys.argv[-3]
db_user = sys.argv[-2]
db_password = sys.argv[-1]

#names of the csv files which contain the data
confs_csv_name = sys.argv[1]
journals_csv_name = sys.argv[2]
persons_csv_name = sys.argv[3]
pubs_csv_name = sys.argv[4]
theses_csv_name = sys.argv[5]


#------------------------------------------------------------------------------------------------------------------------------------------

def csv_to_list(csv_name):

    data_list = []
    with open(csv_name, 'r', encoding='utf-8') as csvfile:
        data_squads = csv.reader(csvfile)

        for row in data_squads:
            #to remove all the ', to have no collisions in the code later on
            new_row = []
            for element in row:
                if isinstance(element, str):
                    element = element.replace("'", "`")
                new_row.append(element)
            #print(new_row)

            data_list.append(new_row)
        #deletes the fist row, which contains the table heads
        #optional:uncomment if this makes working with the data easier for you
        #del data_list[0]
    return data_list

def semicolon_string_to_list(string):
    #interprets all ; of the given string as separator of elements
    #returns a list of strings
    return string.split(';')

#Checks if item is empty, if yes inserts null
def addNull(item):
    if not item:
        item=None
    else:
        item=item;
    return  item

#------------------------------------------------------------------------------------------------------------------------------------------

#Lists from csvs
confs_list = csv_to_list(confs_csv_name)
journals_list = csv_to_list(journals_csv_name)
persons_list = csv_to_list(persons_csv_name)
pubs_list = csv_to_list(pubs_csv_name)
theses_list = csv_to_list(theses_csv_name)

##-----------------------------------------institutions------------------------------------------------------------
#get institutions from person_list
person_inst_list = [item[3:5] for item in persons_list[1:]]
per_inst_list_fin = set(map(tuple,person_inst_list))

#get institutions from theses_list
theses_inst_list = [item[5:7] for item in theses_list[1:]]
theses_inst_list_fin = set(map(tuple,theses_inst_list))

#get institutions from both_lists
institution_list = list(per_inst_list_fin)
institution_list.extend(x for x in theses_inst_list_fin if x not in institution_list)
#remove null values
for item in institution_list:
    if not item[0]:
        institution_list.remove(item)
#
#-----------------------------------------countries---------------------------------------------------------
#get countries from institution list
inst_co_list = [item[1] for item in institution_list]
inst_co_list_fin = list(dict.fromkeys(inst_co_list))
#remove null values
for item in inst_co_list_fin:
    if not item:
        inst_co_list_fin.remove(item)

#get countries from conferences_list
conf_co_list = [item[4] for item in confs_list[1:]]
conf_co_list_fin = list(dict.fromkeys(conf_co_list))

#get countries from both lists
country_list = list(inst_co_list_fin)
country_list.extend(x for x in conf_co_list_fin if x not in inst_co_list_fin)


# #-----------------------------------------Connection---------------------------------------------------------
#
#SQL connection
sql_con = psycopg2.connect(host = db_host, port = db_port, database = db_name, user = db_user, password = db_password)
#cursor, for DB operations
cur = sql_con.cursor()

#-----------------------------------------Countries---------------------------------------------------------

args_str = ','.join(['(%s)'] * len(country_list))
sql = "insert into countries (name) values {}".format(args_str)
cur.execute(cur.mogrify(sql, country_list).decode('utf8'), country_list)

sql =cur.execute("SELECT * FROM countries")
all_countries=cur.fetchall()

all_countries_dict = {item[0]: item[1] for item in all_countries}

#-----------------------------------------Institutions---------------------------------------------------------

all_institutions_fin=[]
tid=None
for item in institution_list:
    for id, name in all_countries_dict.items():
        if name==item[1]:
            tid=id
        elif not item[1]:
            tid=None
    all_institutions_fin.append((item[0], tid))

params = ','.join(['%s'] * len(all_institutions_fin))
sql1 = 'insert into institutions (name, cokey) values {}'.format(params)
cur.execute(sql1, all_institutions_fin)

sql =cur.execute("SELECT * FROM institutions")
all_institutions=cur.fetchall()
all_institutions_dict = {item[0]: item[1] for item in all_institutions}
# #------------------------------------------Persons---------------------------------------------------------
all_persons_fin=[]
ins_id=None
for item in persons_list[1:]:
    item_id=item[0][1:]
    item[5]=addNull(item[5])
    if not item[3]:
        ins_id=None
    else:
        for k,v in all_institutions_dict.items():
            if item[3]==v:
                ins_id=k

    all_persons_fin.append((item_id,item[1],item[5],ins_id))

params = ','.join(['%s'] * len(all_persons_fin))
sql1 = 'insert into persons (akey, name,website,ikey) values {}'.format(params)
cur.execute(sql1, all_persons_fin)

#-----------------------------------------Theses---------------------------------------------------------

all_theses_fin=[]
inst_id=None
for item in theses_list[1:]:
    if not item[5]:
        inst_id=None
    else:
        for k,v in all_institutions_dict.items():
            if item[5]==v:
                inst_id=k

    item_id=item[0][1:]
    item_auth_id=item[1][1:]
    item[3]=addNull(item[3])
    item[4]=addNull(item[4])
    item[8]=addNull(item[8])
    item[7]=addNull(item[7])
    all_theses_fin.append((item_id,item[2],item[3],item[7],item[4],item[8],item_auth_id,inst_id))

params = ','.join(['%s'] * len(all_theses_fin))
sql1 = 'INSERT into Theses(tkey,title,year,npages,type,isbn,akey,ikey) VALUES {}'.format(params)
cur.execute(sql1, all_theses_fin)

#-----------------------------------------Conferences---------------------------------------------------------

all_confs_fin=[]
cid=None
for item in confs_list[1:]:
    for id, name in all_countries_dict.items():
        if name==item[4]:
            cid=id
    item_id=item[0][1:]
    item[7]=addNull(item[7])
    all_confs_fin.append((item_id,item[1],item[2],item[3],cid,item[6],item[7]))

params = ','.join(['%s'] * len(all_confs_fin))
sql1 = 'INSERT into Conferences(ckey,sname,title,city,cokey,year,isbn) VALUES {}'.format(params)
cur.execute(sql1, all_confs_fin)

#-----------------------------------------Journals---------------------------------------------------------
all_journals_fin=[]
for item in journals_list[1:]:
    item_id=item[0][1:]
    item[4]=addNull(item[4])
    all_journals_fin.append((item_id,item[1],item[2],item[3],item[4],item[5]))

params = ','.join(['%s'] * len(all_journals_fin))
sql1 = 'INSERT into Journals(jkey,sname,title,volume,issue,year) VALUES {} '.format(params)
cur.execute(sql1, all_journals_fin)

#-----------------------------------------Papers---------------------------------------------------------
all_pubs_fin=[]
for item in pubs_list[1:]:
    item_id=item[0][1:]
    item[2]=addNull(item[2])
    item_co=None
    item_jour=None
    if item[4].startswith('C'):
        item_co=item[4][1:]
    if item[4].startswith('J'):
        item_jour=item[4][1:]
    all_pubs_fin.append((item_id,item[2],item[3],item_co,item_jour))

params = ','.join(['%s'] * len(all_pubs_fin))
sql1 = 'INSERT into Papers(pkey,title,pages,ckey,jkey) VALUES {} '.format(params)
cur.execute(sql1, all_pubs_fin)

#-----------------------------------------Authpapers---------------------------------------------------------
auth_papers_list=[]
for item in pubs_list[1:]:
    item[0]=item[0][1:]
    dicti=item[1].split(":")
    new_dictionary = dict(enumerate(dicti,start=1))
    for k,v in new_dictionary.items():
        auth_papers_list.append((item[0],v[1:],k))

params = ','.join(['%s'] * len(auth_papers_list))
sql1 = 'INSERT into AuthPapers(pkey,akey,rank) VALUES {} '.format(params)
cur.execute(sql1, auth_papers_list)

# #commit the changes, this makes the database persistent
sql_con.commit()
#close connections
cur.close()
sql_con.close()

print("Program finished...")