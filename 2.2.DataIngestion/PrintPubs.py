#! /usr/bin/env python3
import sys, psycopg2
from operator import itemgetter

sys.argv=sys.argv[1:]
sys.argv.reverse()
db_host = sys.argv[4]
db_port = sys.argv[3]
db_name = sys.argv[2]
db_user = sys.argv[1]
db_password = sys.argv[0]

# #-----------------------------------------Connection---------------------------------------------------------
#
#SQL connection
sql_con = psycopg2.connect(host = db_host, port = db_port, database = db_name, user = db_user, password = db_password)
#cursor, for DB operations
cur = sql_con.cursor()

list_name=sys.argv[5:]
list_name.reverse()
name =" ".join(c for c in list_name)
name1=name.lower()
name2=name1.title()

author_name=name2
cur.execute("Select akey, name from persons where name=%s",[author_name])
res=cur.fetchall()
all_authors_dict = {item[0]: item[1] for item in res}
if not all_authors_dict:
    print("There is no author with the name",author_name,"!")

else:
    auth_id=[key  for (key, value) in all_authors_dict.items() if value == author_name]
    auth_id=auth_id[0]

    cur.execute("Select akey, name from persons")
    res_auth=cur.fetchall()
    res_auth_dict = {item[0]: item[1] for item in res_auth}


    cur.execute("select ckey,sname,year from conferences UNION ALL select jkey,sname,year from journals")
    res_cj=cur.fetchall()
    all_re = {item[0]: item[1:3] for item in res_cj}

    cur.execute("Select pkey from authpapers where akey=%s",[auth_id])
    res2=cur.fetchall()

    def swaplist(list):
        new=[]
        for i in list:
            for k,v in res_auth_dict.items():
                if i==k:
                    new.append(v)
        return new

    all_printed_papers=[]
    for item in res2:
        cur.execute("Select * from papers where pkey=%s",[item])
        res3=cur.fetchall()
        for it in res3:
            pid=it[0]
            cur.execute("Select akey from authpapers where pkey=%s",[pid])
            num=cur.fetchall()
            s = list(sum(num, ()))
            auths_fin=swaplist(s)
            auths_final = str(auths_fin)[1:-1]
            title=it[1]
            pages=it[2]
            if not it[3]:
                cj_id=it[4]
            else:
                cj_id=it[3]
            for k,v in all_re.items():
                if cj_id==k:
                    sname=v[0]
                    year=v[1]
        all_printed_papers.append((auths_final,title,sname,year,pages))

    all_printed_papers.sort(key=itemgetter(3),reverse=True)
    print(author_name, " was an author in ", len(all_printed_papers), "papers:")
    print("            ")
    print(*all_printed_papers,sep="\n")

#commit the changes, this makes the database persistent
sql_con.commit()
#close connections
cur.close()
sql_con.close()

