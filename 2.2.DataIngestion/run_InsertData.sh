#/bin/sh
#installs psycopg2 library, in case it is not installed
pip install psycopg2-binary #you might need to change it to pip, if pip3 is your default pip.
# Run the shell script with the arguments of IngestData(input files and database configuration parameters).
# E.g. bash run_InsertData.sh confs.csv journals.csv persons.csv pubs.csv theses.csv localhost 5432 db11831695 postgres 123
python InsertData.py $1 $2 $3 $4 $5 $6 $7 $8 $9 ${10}

