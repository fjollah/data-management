--3.4 a)

CREATE TABLE R(
	a int,
    b int 
);

CREATE TABLE S(
	a int,
    b int 
);

BEGIN TRANSACTION;

insert into r (a,b) values (2, 4),(3, 5),(6, 8),(7, 9);
insert into s (a,b) values(4, 20),(5, 21),(6, 80);

COMMIT;
-----------------------------------------------------------------------------------------------------
--3.4 b)
-- NOTE: I used the data from the tables in 3.4a)

-- Lowest level that prevents "Phantom Read"
------------------------------------------------------------------------------------------------
BEGIN TRANSACTION ISOLATION LEVEL Repeatable Read;
Select sum(a) from S;
--- Second transaction begins and finishes
Select sum(a) from S;
COMMIT;

BEGIN TRANSACTION ISOLATION LEVEL Repeatable Read;
INSERT INTO S(a,b) values (1,2);
COMMIT;

------------------------------------------------------------------------------------------------

--Highest isolation level that does not prevent "Phantom Read"
BEGIN TRANSACTION ISOLATION LEVEL Read Committed;
Select sum(a) from S;
--- Second transaction begins and finishes
Select sum(a) from S;
COMMIT;

BEGIN TRANSACTION ISOLATION LEVEL Read Committed;
INSERT INTO S(a,b) values (1,2);
COMMIT;

------------------------------------------------------------------------------------------------
-- Order of execution:
-- 1. Start transaction 1
-- 2. Run Select sum(a) query
-- 3. Start transaction 2, insert data and commit
-- 4. Run the same repeated Select sum(a) query in transaction 2
-- 5. Commit

--Explanation

-- The lowest level that prevents the "Phantom Read" anomaly (in Postgresql only) is the Repeatable Read. In this level,
-- a query can only see the data that was committed before starting the transaction, but it cannot see changes made from  
-- other concurrent transactions (even if they are committed). In my example, I start the first transaction and execute a 
-- select query, then I start the second transaction, insert new data and commit it. When I execute the second select 
-- query (the same as the first query) in the first transaction, I will still get the same result as before.
-- This means, that the "Phantom Read" is prevented.
----------------------------------------------------------------------------------------------------------------------------------
-- The highest isolation level that does not prevent(allows) the "Phantom Read" anomaly is the Read Committed. In this
-- level, a select query can see the committed data before the execution of the query, even if this data is in other 
-- concurrent committed transactions. In my example, I execute a select query, then the second transaction begins, inserts
-- new data and commits, and then I run the first select query again. Since the select query can see the committed data
-- before its execution, it will see the inserted row from the second transaction and include that in the result.
-- That is why, when the select query in my example is repeated, the second time we get an answer that is different 
-- from the first (same) query. Thus, resulting in a "Phantom Read" anomaly.
