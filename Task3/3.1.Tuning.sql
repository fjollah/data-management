-- (a) Query Unnesting: Rewrite Q09 below into a semantically equivalent SQL query without
-- any sub queries (i.e., please, unnest the sub-query of the IN-clause).
-- -- Q09:
EXPLAIN ANALYZE SELECT I.Name FROM Institutions I WHERE I.CoKey IN(
SELECT CoKey FROM Countries C WHERE C.Name='Germany' OR C.Name='Austria')

-- Unnested Query
EXPLAIN ANALYZE SELECT I.Name 
FROM Institutions I, Countries C 
WHERE I.CoKey=C.Cokey 
AND (C.Name='Germany' OR C.Name='Austria');

-- Unnesting the query using a join for the respective cokeys, yields a minimal negligible difference in the query plans. The
-- graphical output is the same, the number of times  the query loops through the result set is the same,
-- the intermediate results are the same and even the time costs are roughly the same. The reason behind this, is that  
-- PostgreSQL has an efficient optimizer. The query optimizer has the necessary statistics/data about your database. 
-- For e.g it knows how many tables are there in your database, how many rows in each table etc. With that, the query 
-- optimizer decides which scan to use, which tables to loop through. In our case, since hashing is an expensive process,
-- the query optimizer, in both cases, hashes the countries table (which is smaller) and loops through institutions, 
-- choosing so the most optimized path.

--------------------------------------------------------------------------------------------------------------------

-- (b) Query Rewriting: Rewrite Q10 below into a semantically equivalent SQL query without
-- any intersection or difference operations.
-- -- Q10:
Explain analyze (SELECT P.Name FROM Persons P, Theses T
WHERE P.Akey = T.Akey AND T.Year < 2020)
INTERSECT
(SELECT P.Name FROM Persons P, Theses T
WHERE P.Akey = T.Akey AND T.Year >= 2018)

--Query rewriting
Explain analyze SELECT P.Name FROM Persons P, Theses T
WHERE P.Akey = T.Akey AND 
(T.Year >= 2018 AND T.Year < 2020)

--The query plans between the query 10 and the rewritten form differ signifficantly. In the first query, the theses
-- table is hashed twice, once in the first select query and another time in the second select query. Then on both of
-- the select queries a hash inner join is performed. Afterwards, they both result sets are filtered by the year 
-- conditions and in the end the rows that are present in both sets, produce the final result. On the rewritten query,
-- the table theses is hashed once and one inner join is performed. Afterwards, by applying the year conditions/filters
-- the final result is obtain. Consequently, it is evident that the first query is not so optimized becuase it does the
-- same things twice and this adds up on time costs. Whereas, the rewritten query, there are no repeated operations
-- thus the query execution time is shorter (from 55.64ms in the original query to 21.45ms in the rewritten one).

--------------------------------------------------------------------------------------------------------------------------------

-- (c) Indexing: Add a secondary (non-clustered) index on an attribute of your choosing to
-- speedup the original or rewritten query Q10. Provide the SQL statement for index creation 
-- and again compare the resulting plans.

CREATE INDEX idx_name 
ON persons(name);

SELECT P.Name FROM Persons P, Theses T
WHERE P.Akey = T.Akey 
AND (T.Year >= 2018 AND T.Year < 2020)

-- Considering that p.akey is a primary key of the table persons, it is also implicitly a clustered index. As a result,
-- the best nonclustered index to create was the name attribute. Comparing the query plans, it is clear that creating this
-- index makes the query faster. This becuase in the query plan, we see that the table scan is done roughly 50% faster 
-- (e.g. original query time:0.062 vs query time with index:0.036). As a result, the planning time and execution time
-- decreases as well.
