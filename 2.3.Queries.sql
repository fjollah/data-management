--  Q01: Where did the conference SIGMOD 2019 (short name, year) take place?
-- (return city and country)

SELECT cf.city, co.name as country
from conferences cf 
inner join countries co
on cf.cokey=co.cokey
where (sname='SIGMOD' and year=2019);

-- Result: city:Amsterdam, country:The Netherlands

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

-- • Q02: Which persons are affiliated with Graz University of Technology?
-- (return name, website; sorted ascending by name)

SELECT p.name, p.website
from persons p 
right join institutions i
on p.ikey=i.ikey
where i.name='Graz University of Technology'
ORDER BY p.name ASC;

-- Result:
-- name	                website
-- Alexander Felfernig	http://www.ist.tugraz.at/felfernig/
-- Bernhard C. Geiger	https://orcid.org/0000-0003-3257-743X
-- Denis Helic	        https://orcid.org/0000-0003-0725-7450
-- Hermann A. Maurer	http://www.iicm.edu/Hmaurer
-- Martin Trapp 0001	https://martint.blog/
-- Matthias Boehm 0001	http://matthiasboehm.org/

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- • Q03: How many theses were published per year?
-- (return year, count; sorted ascending by year)

select year, COUNT(*) 
from theses
group by year
ORDER BY year ASC;

-- Result:
-- year	count
-- 2011	187
-- 2012	218
-- 2013	243
-- 2014	259
-- 2015	250
-- 2016	225
-- 2017	224
-- 2018	207
-- 2019	107
-- 2020	8

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- • Q04: Which journal issues contained more than 70 papers?
-- (return title, volume, issue, year; sorted descending by year and issue)

select j.title,j.volume,j.issue,j.year
from journals j
inner join papers p on
j.jkey=p.jkey
group by j.jkey
having count(p.jkey)>70
order by j.year desc, j.issue desc;

-- Result:
-- title	volume	issue	year
-- PVLDB	12	     12	    2019
-- PVLDB	11	     12	    2018
-- PVLDB	8	     12	    2015
-- PVLDB	7	     13	    2014

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- • Q05: Which cities hosted more than 2 conferences?
-- (return city, country, count; sorted decreasing by count)

select c.city, co.name,count(c.city)
from conferences c
inner join countries co on
c.cokey=co.cokey
group by co.name, c.city
having count(c.city)>2
ORDER BY count(c.city) desc;

-- Result:
-- city	            name	        count
-- Chicago	        USA	              6
-- San Francisco	USA	              6
-- Athens	        Greece	          4
-- Beijing	        China	          4
-- Melbourne	    Australia         4
-- New York	        USA	              4
-- Asilomar	        USA	              4
-- Brussels	        Belgium	          4
-- Houston	        USA	              3
-- Amsterdam	    The Netherlands	  3
-- Vienna	        Austria	          3
-- Hong Kong	    China	          3


-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- • Q06: Create a histogram that counts the number of papers for all groups of papers with
-- equal number of authors.
-- (return number of authors, number of papers; sorted ascending by number of authors)

select count(pkey),auths
from 
(SELECT pkey,count(akey) as auths
from authpapers
where pkey=pkey
group by pkey) as temp
group by auths
order by auths asc;

-- Result:
-- count	auths
-- 3354		1
-- 4631		2
-- 6623		3
-- 5509		4
-- 3625 	5
-- 2093	    6
-- 854	    7
-- 401	    8
-- 170	    9
-- 109	    10
-- 68	    11
-- 37	    12
-- 32	    13
-- 28	    14
-- 15	    15
-- 7	    16
-- 5	    17
-- 8	    18
-- 7	    19
-- 8	    20
-- 5	    21
-- 4	    22
-- 4	    23
-- 1	    24
-- 2	    26
-- 2	    27
-- 2	    28
-- 5	    30
-- 1	    33
-- 1	    34
-- 2	    36

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- • Q07: How many distinct theses and papers did persons currently affiliated with Austrian
-- institutions publish? (return single count)

Select count(distinct pkey)
from
(Select t.tkey as pkey, t.akey as akey
from theses t
union all
select ap.pkey as pkey, ap.akey as akey
from authpapers ap) as all_papers
join
(Select akey
from persons p
join
(select ikey, co.name
from institutions i
inner join countries co
on i.cokey=co.cokey
where co.name='Austria') as inst
on p.ikey=inst.ikey) as aus_persons
on all_papers.akey=aus_persons.akey;

-- Result: 233

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- • Q08: How many persons did not publish any journal or conference paper in the year they
-- published their PhD thesis? (return single count)

Select COUNT(distinct ok.akey)
from
(Select distinct tot_auth.akey
from
(Select distinct ap.akey, pap.year
from authpapers ap
join
(Select *
from
(SELECT distinct p.pkey, c.year as year
FROM papers p
INNER JOIN conferences c
on p.ckey=c.ckey) as con
union
select *
from
(SELECT distinct p.pkey, j.year as year
FROM papers p
INNER JOIN journals j
on p.jkey=j.jkey) as jour) as pap
on ap.pkey=pap.pkey) as tot_auth
join
(Select akey, year
from theses
where type='PhD') as t
on tot_auth.akey=t.akey) as ok
where ok.akey NOT IN
-- all people who have done papers and phd in the same year
(Select distinct tot_auth.akey
from
(Select distinct ap.akey, pap.year
from authpapers ap
join
(Select *
from
(SELECT distinct p.pkey, c.year as year
FROM papers p
INNER JOIN conferences c
on p.ckey=c.ckey) as con
union
select *
from
(SELECT distinct p.pkey, j.year as year
FROM papers p
INNER JOIN journals j
on p.jkey=j.jkey) as jour) as pap
on ap.pkey=pap.pkey) as tot_auth
join
(Select akey, year
from theses
where type='PhD') as t
on tot_auth.akey=t.akey
where t.year=tot_auth.year);

-- Result:1304